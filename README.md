# typeahead-demo

This project uses Angular for the frontend and Node.js with GraphQL for the backend to create a typeahead search input for all American states and territories. Use Google Map API to display the US map and highlight areas based on selected search result.

## Getting Started

### Prerequisites

Ensure you have the following installed on your local machine:

- [Node.js & npm](https://nodejs.org/en/download/)
- [Angular CLI](https://cli.angular.io/)
- [Apollo Server](https://www.apollographql.com/docs/apollo-server/)

### Installation

1. Clone the repo:
    ```bash
    git clone https://gitlab.com/yangfy/typeahead-demo
    ```

2. Install NPM packages and run the backend server:
    ```bash
    cd typeaheadServer
    npm install
    node server.js
    ```
    Ensure the server is running on `http://localhost:3000`.
    
3. Install NPM packages and run the frontend server:
    ```bash
    cd typeaheadClient
    npm install
    ng serve
    ```
    Navigate to `http://localhost:4200` on your browser to see the application in action.

## Usage

This is what the page looks like:

<img src="./img/page.png" alt="img/" width="800" />

Use the search bar to find and select an American state or territory. As you type, you will see suggestions based on your input.

<img src="./img/searchbar.jpg" alt="img/" width="300" />



After clicking an item from the search bar, there will be a highlight area on the selected state.

<img src="./img/selectmap.png" alt="img/" width="800" />

## Acknowledgments

* List of states and territories of the United States: https://gist.github.com/mrcsmcln/91e49b1f4eadb21099ce
* Download the states' boundaries coordinates data of US states and territories from [GADM](https://gadm.org/download_country_v3.html#google_vignette).

