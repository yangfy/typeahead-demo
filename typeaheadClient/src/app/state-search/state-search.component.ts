import { Component, ViewChild, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { StateSearchService } from '../state-search.service';
import { Subject } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';

declare var google: any;

@Component({
  selector: 'app-state-search',
  templateUrl: './state-search.component.html',
  styleUrls: ['./state-search.component.css'],
})
export class StateSearchComponent implements OnInit, AfterViewInit {
  @ViewChild('mapContainer', { static: false }) gmap!: ElementRef;

  term: string = "";
  searchTerms = new Subject<string>();
  states: any = [];
  selectedState: any = null;
  polygon: any = null;

  statesCoordinates: any = [];

  map: any;

  selectState(state: any){
    this.term = state.name;
    this.states = [];
    console.log(this.statesCoordinates);
    let coordinate = this.statesCoordinates.find((item: any) => {
      if (item.name == state.name) return true;
      else return false;
    });
    console.log(coordinate);

    if (this.polygon != null){
      this.polygon.forEach((element: any) => {
        element.setMap(null);
      });
    }
      
    this.polygon = coordinate.coordinates.map((element: any) => 
      (new google.maps.Polygon({
        paths: element,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 3,
        fillColor: '#FF0000',
        fillOpacity: 0.35
      })));
    
      console.log(this.polygon);

    // Draw the polygon on the desired map instance
    this.polygon.forEach((element: any) => {
      element.setMap(this.map);
    });
    //this.polygon.setMap(this.map);
    // console.log(a);
  }

  ngOnInit() {
    this.http.get('assets/statesCoordinates.json').subscribe((data) => {
      this.statesCoordinates = data;
    });
  }

  ngAfterViewInit() {
    this.map = new google.maps.Map(this.gmap.nativeElement, {
      center: { lat: 37.0902, lng: -95.7129 }, // Initial position (center of USA)
      zoom: 4
    });
  }

  constructor(private stateSearchService: StateSearchService, private http: HttpClient) {
    this.searchTerms.pipe(
      debounceTime(300),
      switchMap((term: string) => this.stateSearchService.searchStates(term))
    ).subscribe((result: any) => {
      this.states = result.data && result.data.searchStates;
    });
  }
}
